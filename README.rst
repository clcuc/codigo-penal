**********
Penal Code
**********

This sourcecode builds a website that displays the Penal Code of Peru, and that also includes a
toggle switch to choose between light mode and dark mode. 

Website
=======

The URL is 
https://clcuc.gitlab.io/codigo-penal

Building
========

First, ``git clone`` this repository, and then on your terminal follow the steps shown in the
``.gitlab-ci.yml`` file. 
